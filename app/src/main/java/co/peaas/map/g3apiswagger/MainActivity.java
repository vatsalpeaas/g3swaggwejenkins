package co.peaas.map.g3apiswagger;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.testfairy.TestFairy;

import org.junit.runner.JUnitCore;

import java.io.IOException;

import co.peaas.map.g3apiswagger.api.DefaultApi;
import co.peaas.map.g3apiswagger.model.InitProvisionRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionResponse;
import co.peaas.map.g3apiswagger.model.ResendOTPRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  {

    private DefaultApi defaultApi;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_call_api = (Button) findViewById(R.id.btn_call_api);

        defaultApi = new ApiClient().createService(DefaultApi.class);


        TestFairy.begin(this, "04426b8518bb6e1dba845b9bf9f478fe31a4e881");




        btn_call_api.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JUnitCore junit = new JUnitCore();
                junit.run(TestAllApiActivity.class);






      /*          InitProvisionRequest init = new InitProvisionRequest();
               init.setApplicationVersion("1.0.2");
                init.setContactPhone("4168791223");
                init.setCountryCode("CA");
               init.setDeviceID("959595959595959");
                init.setEmail("mobiledev@peaas.co");
                init.setPlatform("Android");

                Call<InitProvisionResponse> requestCall = defaultApi.initProvisioningPost(init);
                try {
                    Response<InitProvisionResponse> response = requestCall.execute();
                    Log.d("MainActivity","onResponse"+response.toString());
                    Log.d("MainActivity","onResponse"+response.body());
                    Log.d("MainActivity","onResponse"+response.body().getEmail());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                requestCall.enqueue(new Callback<InitProvisionResponse>() {
                   @Override
                   public void onResponse(Call<InitProvisionResponse> call, Response<InitProvisionResponse> response) {
                       Log.d("MainActivity","onResponse"+response.toString());
                       Log.d("MainActivity","onResponse"+response.body());
                       Log.d("MainActivity","onResponse"+response.body().getEmail());
                   }

                    @Override
                   public void onFailure(Call<InitProvisionResponse> call, Throwable t) {
                        t.printStackTrace();
                        Log.d("MainActivity","onFailure");
                    }
               });
*/

//                ResendOTPRequest request = new ResendOTPRequest();
//                request.setCustomerId("471293");
//                request.setContactPhone("4168791223");
//                // ResendOTPResponse response = api.resendOtpPost(init);
//
//                Call<ResendOTPResponse> response = defaultApi.resendOtpPost(request);
//
//
//                response.enqueue(new Callback<ResendOTPResponse>() {
//                    @Override
//                    public void onResponse(Call<ResendOTPResponse> call, Response<ResendOTPResponse> response) {
//                        Log.d("MainActivity","onResponse"+response.toString());
//                        Log.d("MainActivity","onResponse"+response.body());
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResendOTPResponse> call, Throwable t) {
//                        t.printStackTrace();
//                        Log.d("MainActivity","onFailure");
//                    }
//                });
//
//
//            }
//        });


//
//
//                GetProfileInfoRequest request = new GetProfileInfoRequest();
//                request.setCustomerId("471293");
//
//
//                Call<GetProfileInfoResponse> response = defaultApi.getProfileInfoPost(request);
//
//
//                response.enqueue(new Callback<GetProfileInfoResponse>() {
//                    @Override
//                    public void onResponse(Call<GetProfileInfoResponse> call, Response<GetProfileInfoResponse> response) {
//                        Log.d("MainActivity", "onResponse" + response.toString());
//                        Log.d("MainActivity", "onResponse" + response.body());
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<GetProfileInfoResponse> call, Throwable t) {
//                        t.printStackTrace();
//                        Log.d("MainActivity", "onFailure");
//                    }
//                });


            }
        });

    }




}