package co.peaas.map.g3apiswagger;

import android.app.Activity;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import co.peaas.map.g3apiswagger.api.DefaultApi;
import co.peaas.map.g3apiswagger.model.AddCreditCardRequest;
import co.peaas.map.g3apiswagger.model.AddCreditCardResponse;
import co.peaas.map.g3apiswagger.model.Address;
import co.peaas.map.g3apiswagger.model.CCAddress;
import co.peaas.map.g3apiswagger.model.ChangeProfileRequest;
import co.peaas.map.g3apiswagger.model.ChangeProfileResponse;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentRequest;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentResponse;
import co.peaas.map.g3apiswagger.model.CreditCard;
import co.peaas.map.g3apiswagger.model.EmailVerificationRequest;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsByStateRequest;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsRequest;
import co.peaas.map.g3apiswagger.model.GetAccountHistoryRequest;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRespone;
import co.peaas.map.g3apiswagger.model.GetAllCreditCardsRequest;
import co.peaas.map.g3apiswagger.model.GetBalanceRequest;
import co.peaas.map.g3apiswagger.model.GetCdrRequest;
import co.peaas.map.g3apiswagger.model.GetConfigurationsRequest;
import co.peaas.map.g3apiswagger.model.GetCountryBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetLanRequest;
import co.peaas.map.g3apiswagger.model.GetLanResponse;
import co.peaas.map.g3apiswagger.model.GetLocalAccessNumberRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoIAPRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentMethodRequestRequest;
import co.peaas.map.g3apiswagger.model.GetProfileInfoRequest;
import co.peaas.map.g3apiswagger.model.GetProfileInfoResponse;
import co.peaas.map.g3apiswagger.model.GetRateRequest;
import co.peaas.map.g3apiswagger.model.GetRechargeAmountsRequest;
import co.peaas.map.g3apiswagger.model.GetSpecialOfferRequest;
import co.peaas.map.g3apiswagger.model.GetWorldBundlesRequest;
import co.peaas.map.g3apiswagger.model.IAPTopUpRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionResponse;
import co.peaas.map.g3apiswagger.model.LocationInfo;
import co.peaas.map.g3apiswagger.model.MobileInfo;
import co.peaas.map.g3apiswagger.model.RedeemVoucherRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendResponse;
import co.peaas.map.g3apiswagger.model.ResendEmailRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPResponse;
import co.peaas.map.g3apiswagger.model.SetUpCallForwardRequest;
import co.peaas.map.g3apiswagger.model.UpdatePushTokenRequest;
import co.peaas.map.g3apiswagger.model.VerifyOtpRequest;
import co.peaas.map.g3apiswagger.model.VerifyOtpResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * API tests for DefaultApi
 */

public class TestAllApiActivity {

    private DefaultApi api;

    private MainActivity mainActivity;

    @Before
    public void setup() {
        api = new ApiClient().createService(DefaultApi.class);
       // callallapi = new Callallapi();
        mainActivity = new MainActivity();
    }


    /**
     * Add credit card info
     * <p>
     * Returns credit card info.
     */
    @Test
    public void addCreditCardPostTest() throws IOException {
        // TODO: test validations


        AddCreditCardRequest addCreditCard = new AddCreditCardRequest();
        CCAddress ccAddress = new CCAddress();
        ccAddress.setSelectedAddress("440867");
        CreditCard creditCard = new CreditCard();
        creditCard.setCcExpMonth("05");
        creditCard.setCcExpYear("2022");
        creditCard.setCcNumber("4242424242424242");
        creditCard.setCcOwnerName("Test Credit Card");
        creditCard.setCcvNumber("753");
        addCreditCard.setCreditCard(creditCard);
        addCreditCard.setAddress(ccAddress);
        addCreditCard.setCustomerId("504745");
        addCreditCard.setSelectedCC("Visa");
        addCreditCard.setAni("9011114500");
        addCreditCard.setApplicationVersion("1.0.3");
        addCreditCard.setDeviceID("trdef8b4b75cb110");
        addCreditCard.setPlatform("Android");

        // AddCreditCardResponse response = api.addCreditCardPost(addCreditCard);

        Call<AddCreditCardResponse> response = api.addCreditCardPost(addCreditCard);

        response.enqueue(new Callback<AddCreditCardResponse>() {
            @Override
            public void onResponse(Call<AddCreditCardResponse> call, Response<AddCreditCardResponse> response) {

                Log.d("AddCreditCard","onResponse"+response.body());

            }
            @Override
            public void onFailure(Call<AddCreditCardResponse> call, Throwable t) {
            t.printStackTrace();
            }
        });

    }

    /**
     * changeProfile
     * <p>
     * Returns user profile related information.
     */
    @Test
    public void changeProfilePostTest() throws IOException {
        // TODO: test validations

        ChangeProfileRequest init = new ChangeProfileRequest();
        Address address = new Address();
        address.setSelectedAddress("439901");
        init.setAddress(address);
        init.setCustomerId("504745");
        init.setContactPhone("8866114543");
        init.setEmail("dot@com.com");
        init.setFirstName("Aaa");
        init.setLastName("Hhhhhh");
        init.setAni("8866114543");
        init.setApplicationVersion("1.0.3");
        init.setDeviceID("30e5djb4b75cb110");
        init.setPlatform("Android");
        // ChangeProfileResponse response = api.changeProfilePost(init);

        Call<ChangeProfileResponse> response = api.changeProfilePost(init);

        response.enqueue(new Callback<ChangeProfileResponse>() {
            @Override
            public void onResponse(Call<ChangeProfileResponse> call, Response<ChangeProfileResponse> response) {
                Log.d("changeProfile","onResponse"+response.body());
            }

            @Override
            public void onFailure(Call<ChangeProfileResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });


//        Log.d("initProvisioning", "onResponse" + responseR.body());

    }

    /**
     * ConfirmPayment
     * <p>
     * Returns a Confirm Payment related information.
     */
    @Test
    public void confirmPaymentPostTest() throws IOException {
        ConfirmPaymentRequest iaPTopUpRequest = new ConfirmPaymentRequest();
        iaPTopUpRequest.setCustomerID("471286");
        // ConfirmPaymentResponse response = api.confirmPaymentPost(iaPTopUpRequest);

        Call<ConfirmPaymentResponse> response = api.confirmPaymentPost(iaPTopUpRequest);
        response.enqueue(new Callback<ConfirmPaymentResponse>() {
            @Override
            public void onResponse(Call<ConfirmPaymentResponse> call, Response<ConfirmPaymentResponse> response) {
                Log.d("ConfirmPayment", "onResponse" + response.body());
            }

            @Override
            public void onFailure(Call<ConfirmPaymentResponse> call, Throwable t) {
              t.printStackTrace();
            }
        });



        // TODO: test validations
    }

    /**
     * Email Verification Check
     * <p>
     * Email Verification Check
     */
    @Test
    public void emailVerificationCheckPostTest() {
        EmailVerificationRequest emailVerificationCheck = null;
        // EmailVerificationResponse response = api.emailVerificationCheckPost(emailVerificationCheck);

        // TODO: test validations
    }

    /**
     * GetAccessLocationsByState
     * <p>
     * Returns location related information.
     */
    @Test
    public void getAccessLocationsByStatePostTest() {
        GetAccessLocationsByStateRequest getAccessLocationsByState = null;
        // GetAccessLocationsResponse response = api.getAccessLocationsByStatePost(getAccessLocationsByState);

        // TODO: test validations
    }

    /**
     * GetAccessLocations
     * <p>
     * Returns location related information.
     */
    @Test
    public void getAccessLocationsPostTest() {
        GetAccessLocationsRequest getAccessLocations = null;
        // GetAccessLocationsResponse response = api.getAccessLocationsPost(getAccessLocations);

        // TODO: test validations
    }

    /**
     * GetAccountHistoryRequest
     * <p>
     * Returns a Account History related information.
     */
    @Test
    public void getAccountHistoryPostTest() {
        GetAccountHistoryRequest getAccountHistoryRequest = null;
        // GetAccountHistoryResponse response = api.getAccountHistoryPost(getAccountHistoryRequest);

        // TODO: test validations
    }

    /**
     * GetAllBundles
     * <p>
     * Returns bundles related information.
     */
    @Test
    public void getAllBundlesPostTest() {
        GetAllBundlesRequest getAllBundlesRequest = new GetAllBundlesRequest();
           getAllBundlesRequest.setCustomerID("490571");
        getAllBundlesRequest.setAni("9277208880");

        Call<GetAllBundlesRespone> respoanse = api.getAllBundlesPost(getAllBundlesRequest);

        respoanse.enqueue(new Callback<GetAllBundlesRespone>() {
            @Override
            public void onResponse(Call<GetAllBundlesRespone> call, Response<GetAllBundlesRespone> response) {
                Log.d("GetAllBundles", "onResponse" + response.body());
            }

            @Override
            public void onFailure(Call<GetAllBundlesRespone> call, Throwable t) {
                t.printStackTrace();

            }
        });
        // GetAllBundlesRespone response = api.getAllBundlesPost(init);

        // TODO: test validations
    }

    /**
     * to get allcreditcardinfo
     * <p>
     * Returns credit card info
     */
    @Test
    public void getAllCreditCardsPostTest() {
        GetAllCreditCardsRequest getAllCreditCards = null;
        // GetAllCreditCardsResponse response = api.getAllCreditCardsPost(getAllCreditCards);

        // TODO: test validations
    }

    /**
     * to get Rechargeamount
     * <p>
     * for get Recharge amount
     */
    @Test
    public void getAutoRechargeAmountsPostTest() {
        GetRechargeAmountsRequest getAutoRechargeAmounts = null;
        // GetRechargeAmountsResponse response = api.getAutoRechargeAmountsPost(getAutoRechargeAmounts);

        // TODO: test validations
    }

    /**
     * GetBalance
     * <p>
     * Returns balance related information.
     */
    @Test
    public void getBalancePostTest() {
        GetBalanceRequest getBalance = new GetBalanceRequest();
        // GetBalanceResponse response = api.getBalancePost(getBalance);

        // TODO: test validations
    }

    /**
     * GetCdr
     * <p>
     * Returns Cdr related information.
     */
    @Test
    public void getCdrPostTest() {
        GetCdrRequest getCdr = null;
        // GetCdrResponse response = api.getCdrPost(getCdr);

        // TODO: test validations
    }

    /**
     * Get Configurations
     * <p>
     * GetConfigurations
     */
    @Test
    public void getConfigurationsPostTest() {
        GetConfigurationsRequest getConfigurations = null;
        // GetConfigurationsResponse response = api.getConfigurationsPost(getConfigurations);

        // TODO: test validations
    }

    /**
     * to get countrybundle
     * <p>
     * Returns countrybundle
     */
    @Test
    public void getCountryBundlesPostTest() {
        GetCountryBundlesRequest getCountryBundles = null;
        // GetCountryBundlesResponse response = api.getCountryBundlesPost(getCountryBundles);

        // TODO: test validations
    }

    /**
     * GetLan
     * <p>
     * Get LAN INfo.
     */
    @Test
    public void getLanPostTest() {
        GetLanRequest getLanRequest = new GetLanRequest();
        getLanRequest.setCustomerId("511493");

        MobileInfo mobileInfo = new MobileInfo();
        mobileInfo.setAni("511493");
        mobileInfo.setCountry("CA");
        mobileInfo.setDevicePin("1234567890");
        mobileInfo.setDeviceMake("Android");
        mobileInfo.setDeviceProfile("5.1.1");
        mobileInfo.setApplicationVersion("2.0");
        mobileInfo.setCareer("Vodafone");
        mobileInfo.setAuthcode("ABC");
        mobileInfo.setNpa("416879");
        mobileInfo.emailId("mobiledev@peaas.co");
        mobileInfo.setXmlVersion("4.0");

        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setLat("40.748817");
        locationInfo.setLng("-73.985428");

         getLanRequest.setMobileInfoModel(mobileInfo);
        getLanRequest.setLocationInfo(locationInfo);

        Call<GetLanResponse> response = api.getLanPost(getLanRequest);

        response.enqueue(new Callback<GetLanResponse>() {
            @Override
            public void onResponse(Call<GetLanResponse> call, Response<GetLanResponse> response) {
                Log.d("GetLan","onResponse"+ response.body());
            }

            @Override
            public void onFailure(Call<GetLanResponse> call, Throwable t) {
               t.printStackTrace();
            }
        });
        // GetLanResponse response = api.getLanPost(getLanRequest);

        // TODO: test validations
    }

    /**
     * GetLocalAccessNumber
     * <p>
     * Returns LocalAccessNumber related information.
     */
    @Test
    public void getLocalAccessNumberPostTest() {
        GetLocalAccessNumberRequest getLocalAccessNumber = null;
        // GetLocalAccessNumberResponse response = api.getLocalAccessNumberPost(getLocalAccessNumber);

        // TODO: test validations
    }

    /**
     * Ge tPayment Info InApp
     * <p>
     * Get Payment Info.
     */
    @Test
    public void getPaymentInfoInAppPostTest() {
        GetPaymentInfoIAPRequest getPaymentInfoInApp = null;
        // GetPaymentInfoIAPResponse response = api.getPaymentInfoInAppPost(getPaymentInfoInApp);

        // TODO: test validations
    }

    /**
     * GetPaymentInfo
     * <p>
     * Returns a Payment Info related information.
     */
    @Test
    public void getPaymentInfoPostTest() {
        GetPaymentInfoRequest getPaymentInfoRequest = null;
        // GetPaymentInfoResponse response = api.getPaymentInfoPost(getPaymentInfoRequest);

        // TODO: test validations
    }

    /**
     * GetPaymentMethod
     * <p>
     * Returns a Payment Method related information.
     */
    @Test
    public void getPaymentMethodPostTest() {
        GetPaymentMethodRequestRequest getPaymentMethodRequestRequest = null;
        // GetPaymentMethodResponse response = api.getPaymentMethodPost(getPaymentMethodRequestRequest);

        // TODO: test validations
    }

    /**
     * GetProfileInfoRequest
     * <p>
     * Returns a user Profile related information.
     */
    @Test
    public void getProfileInfoPostTest() throws IOException {
        GetProfileInfoRequest getProfileInfoRequest = new GetProfileInfoRequest();
        getProfileInfoRequest.setCustomerId("490571");

        Call<GetProfileInfoResponse> response = api.getProfileInfoPost(getProfileInfoRequest);

      response.enqueue(new Callback<GetProfileInfoResponse>() {
         @Override
         public void onResponse(Call<GetProfileInfoResponse> call, Response<GetProfileInfoResponse> response) {
             Log.d("GetProfileInfoRequest", "onResponse" + response.body());
         }

         @Override
         public void onFailure(Call<GetProfileInfoResponse> call, Throwable t) {
          t.printStackTrace();
         }
     });

        // TODO: test validations
    }

    /**
     * GetRate
     * <p>
     * Returns Rate related information.
     */
    @Test
    public void getRatePostTest() {
        GetRateRequest getRate = null;
        // GetRateResponse response = api.getRatePost(getRate);

        // TODO: test validations
    }

    /**
     * to get Rechargeamount
     * <p>
     * for get Recharge amount
     */
    @Test
    public void getRechargeAmountsPostTest() {
        GetRechargeAmountsRequest getRechargeAmounts = null;
        // GetRechargeAmountsResponse response = api.getRechargeAmountsPost(getRechargeAmounts);

        // TODO: test validations
    }

    /**
     * GetSpecialOfferRequest
     * <p>
     * Returns a Special Offer related information.
     */
    @Test
    public void getSpecialOfferPostTest() {
        GetSpecialOfferRequest getSpecialOfferRequest = null;
        // GetProfileInfoResponse response = api.getSpecialOfferPost(getSpecialOfferRequest);

        // TODO: test validations
    }

    /**
     * Get World Bundles
     * <p>
     * Get World Bundles
     */
    @Test
    public void getWorldBundlesPostTest() {
        GetWorldBundlesRequest getWorldBundles = null;
        // GetWorldBundlesResponse response = api.getWorldBundlesPost(getWorldBundles);

        // TODO: test validations
    }

    /**
     * IAP Subscribe
     * <p>
     * IAP Subscribe
     */
    @Test
    public void iAPSubscribePostTest() {
        IAPTopUpRequest iaPSubscribe = null;
        // ConfirmPaymentResponse response = api.iAPSubscribePost(iaPSubscribe);

        // TODO: test validations
    }

    /**
     * InApp Topup
     * <p>
     * InApp Topup
     */
    @Test
    public void iAPTopUpPostTest() {
        IAPTopUpRequest iaPTopUp = null;
        // ConfirmPaymentResponse response = api.iAPTopUpPost(iaPTopUp);

        // TODO: test validations
    }

    /**
     * InitProvisioning
     * <p>
     * Returns a user related information.
     */
    @Test
    public void initProvisioningPostTest() throws IOException {
        // TODO: test validations

        InitProvisionRequest init = new InitProvisionRequest();
        init.setApplicationVersion("1.0.2");
        init.setContactPhone("4168791223");
        init.setCountryCode("CA");
        init.setDeviceID("959595959595959");
        init.setEmail("mobiledev@peaas.co");
        init.setPlatform("Android");
        // InitProvisionResponse response = api.initProvisioningPost(init);
       Call<InitProvisionResponse> response = api.initProvisioningPost(init);

        response.enqueue(new Callback<InitProvisionResponse>() {
            @Override
            public void onResponse(Call<InitProvisionResponse> call, Response<InitProvisionResponse> response) {

                Log.d("InitProvisioning","onResponse"+response.body());

            }

            @Override
            public void onFailure(Call<InitProvisionResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });



    }

    /**
     * InitTopUp
     * <p>
     * Returns a Init Top Up related information.
     */
    @Test
    public void initTopUpPostTest() {
        IAPTopUpRequest iaPTopUpRequest = null;
        // InitTopUpResponse response = api.initTopUpPost(iaPTopUpRequest);

        // TODO: test validations
    }

    /**
     * RedeemVoucher
     * <p>
     * Returns a Redeem Voucher related information.
     */
    @Test
    public void redeemVoucherPostTest() {
        RedeemVoucherRequest redeemVoucherRequest = null;
        // ConfirmPaymentResponse response = api.redeemVoucherPost(redeemVoucherRequest);

        // TODO: test validations
    }

    /**
     * ReferFriend
     * <p>
     * Returns confirmation message about refer a friend.
     */
    @Test
    public void referFriendPostTest() {
        ReferFriendRequest referFriendRequest =new ReferFriendRequest();
        referFriendRequest.setCustomerID("471295");
        referFriendRequest.setFriendContactPhone("8238980348");
        referFriendRequest.setFriendEmail("mamta.karmani@gmail.com");
        referFriendRequest.setFriendFirstName("Mamta");
        referFriendRequest.setFriendLastName("Karmani");


        Call<ReferFriendResponse> response = api.referFriendPost(referFriendRequest);

        response.enqueue(new Callback<ReferFriendResponse>() {
            @Override
            public void onResponse(Call<ReferFriendResponse> call, Response<ReferFriendResponse> response) {
                Log.d("ReferFriend","onResponse"+response.body());
            }

            @Override
            public void onFailure(Call<ReferFriendResponse> call, Throwable t) {
               t.printStackTrace();
            }
        });
        // ReferFriendResponse response = api.referFriendPost(init);

        // TODO: test validations
    }

    /**
     * Resend Email
     * <p>
     * ResendEmail
     */
    @Test
    public void resendEmailPostTest() {
        ResendEmailRequest resendEmail = null;
        // ResendEmailResponse response = api.resendEmailPost(resendEmail);

        // TODO: test validations
    }

    /**
     * ResendOtp
     * <p>
     * Returns a OTP related information.
     */
    @Test
    public void resendOtpPostTest() throws IOException {
        ResendOTPRequest init = new ResendOTPRequest();
        init.setCustomerId("471293");
        init.setContactPhone("4168791223");
        // ResendOTPResponse response = api.resendOtpPost(init);

        Call<ResendOTPResponse> response = api.resendOtpPost(init);

        response.enqueue(new Callback<ResendOTPResponse>() {
            @Override
            public void onResponse(Call<ResendOTPResponse> call, Response<ResendOTPResponse> response) {
                Log.d("ResendOtp","onResponse"+ response.body());
            }

            @Override
            public void onFailure(Call<ResendOTPResponse> call, Throwable t) {
               t.printStackTrace();
            }
        });
//            Log.d("initProvisioning", "onResponse" + responseR.body());


        // TODO: test validations
    }

    /**
     * to setupcallforwarding
     * <p>
     * for set call forwarding
     */
    @Test
    public void setUpCallForwardPostTest() {
        SetUpCallForwardRequest setUpCallForward = null;
        // SetUpCallForwardResponse response = api.setUpCallForwardPost(setUpCallForward);

        // TODO: test validations
    }

    /**
     * UpdatePushToken
     * <p>
     * Returns push token related information.
     */
    @Test
    public void updatePushTokenPostTest() {
        UpdatePushTokenRequest updatePushToken = null;
        // UpdatePushTokenResponse response = api.updatePushTokenPost(updatePushToken);

        // TODO: test validations
    }

    /**
     * verifyOtp
     * <p>
     * Returns a OTP related information.
     */
    @Test
    public void verifyOtpPostTest() {
        VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest();
        verifyOtpRequest.setOtp("406099");
        verifyOtpRequest.setCustomerId(471293);


        MobileInfo mobileInfo = new MobileInfo();
        mobileInfo.setAni("4168791223");
        mobileInfo.setCountry("US");
        mobileInfo.setDevicePin("1234567890");
        mobileInfo.setDeviceMake("Android");
        mobileInfo.setDeviceProfile("5.1.1");
        mobileInfo.setApplicationVersion("1.0.2");
        mobileInfo.setCareer("Vodafone");
        mobileInfo.setAuthcode("ABC");
        mobileInfo.setNpa("646785");
        mobileInfo.emailId("mobiledev@peaas.co");
        mobileInfo.setXmlVersion("4.0");

          verifyOtpRequest.setMobileInfoModel(mobileInfo);

        Call<VerifyOtpResponse> response = api.verifyOtpPost(verifyOtpRequest);
        response.enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                Log.d("Verifyotp","onResponse"+ response.body());
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                  t.printStackTrace();
            }
        });
        // VerifyOtpResponse response = api.verifyOtpPost(init);

        // TODO: test validations
    }

}
