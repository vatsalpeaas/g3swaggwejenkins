/*
 * G3 API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package co.peaas.map.g3apiswagger.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * GetPaymentInfoIAPResponse
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-11-09T12:19:59.555+05:30")
public class GetPaymentInfoIAPResponse extends CommonAPIResponse {
  @SerializedName("isTopUp")
  private Boolean isTopUp = null;

  @SerializedName("inAppProductModels")
  private List<InAppProduct> inAppProductModels = new ArrayList<InAppProduct>() ;

  @SerializedName("bundleWebCode")
  private Integer bundleWebCode = null;

  @SerializedName("ani")
  private String ani = null;

  public GetPaymentInfoIAPResponse isTopUp(Boolean isTopUp) {
    this.isTopUp = isTopUp;
    return this;
  }

   /**
   * Get isTopUp
   * @return isTopUp
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getIsTopUp() {
    return isTopUp;
  }

  public void setIsTopUp(Boolean isTopUp) {
    this.isTopUp = isTopUp;
  }

  public GetPaymentInfoIAPResponse inAppProductModels(List<InAppProduct> inAppProductModels) {
    this.inAppProductModels = inAppProductModels;
    return this;
  }

  public GetPaymentInfoIAPResponse addInAppProductModelsItem(InAppProduct inAppProductModelsItem) {
    if (this.inAppProductModels == null) {
      this.inAppProductModels = new ArrayList<InAppProduct>();
    }
    this.inAppProductModels.add(inAppProductModelsItem);
    return this;
  }

   /**
   * Get inAppProductModels
   * @return inAppProductModels
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<InAppProduct> getInAppProductModels() {
    return inAppProductModels;
  }

  public void setInAppProductModels(List<InAppProduct> inAppProductModels) {
    this.inAppProductModels = inAppProductModels;
  }

  public GetPaymentInfoIAPResponse bundleWebCode(Integer bundleWebCode) {
    this.bundleWebCode = bundleWebCode;
    return this;
  }

   /**
   * Get bundleWebCode
   * @return bundleWebCode
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getBundleWebCode() {
    return bundleWebCode;
  }

  public void setBundleWebCode(Integer bundleWebCode) {
    this.bundleWebCode = bundleWebCode;
  }

  public GetPaymentInfoIAPResponse ani(String ani) {
    this.ani = ani;
    return this;
  }

   /**
   * Get ani
   * @return ani
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getAni() {
    return ani;
  }

  public void setAni(String ani) {
    this.ani = ani;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetPaymentInfoIAPResponse getPaymentInfoIAPResponse = (GetPaymentInfoIAPResponse) o;
    return Objects.equals(this.isTopUp, getPaymentInfoIAPResponse.isTopUp) &&
        Objects.equals(this.inAppProductModels, getPaymentInfoIAPResponse.inAppProductModels) &&
        Objects.equals(this.bundleWebCode, getPaymentInfoIAPResponse.bundleWebCode) &&
        Objects.equals(this.ani, getPaymentInfoIAPResponse.ani) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isTopUp, inAppProductModels, bundleWebCode, ani, super.hashCode());
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetPaymentInfoIAPResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    isTopUp: ").append(toIndentedString(isTopUp)).append("\n");
    sb.append("    inAppProductModels: ").append(toIndentedString(inAppProductModels)).append("\n");
    sb.append("    bundleWebCode: ").append(toIndentedString(bundleWebCode)).append("\n");
    sb.append("    ani: ").append(toIndentedString(ani)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

