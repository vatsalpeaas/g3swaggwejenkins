/*
 * G3 API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package co.peaas.map.g3apiswagger.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * InAppProduct
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-11-09T12:19:59.555+05:30")
public class InAppProduct {
  @SerializedName("g3Amount")
  private Double g3Amount = null;

  @SerializedName("inAppAmount")
  private Double inAppAmount = null;

  @SerializedName("inAppProductID")
  private String inAppProductID = null;

  @SerializedName("displayValue")
  private String displayValue = null;

  public InAppProduct g3Amount(Double g3Amount) {
    this.g3Amount = g3Amount;
    return this;
  }

   /**
   * Get g3Amount
   * @return g3Amount
  **/
  @ApiModelProperty(example = "null", value = "")
  public Double getG3Amount() {
    return g3Amount;
  }

  public void setG3Amount(Double g3Amount) {
    this.g3Amount = g3Amount;
  }

  public InAppProduct inAppAmount(Double inAppAmount) {
    this.inAppAmount = inAppAmount;
    return this;
  }

   /**
   * Get inAppAmount
   * @return inAppAmount
  **/
  @ApiModelProperty(example = "null", value = "")
  public Double getInAppAmount() {
    return inAppAmount;
  }

  public void setInAppAmount(Double inAppAmount) {
    this.inAppAmount = inAppAmount;
  }

  public InAppProduct inAppProductID(String inAppProductID) {
    this.inAppProductID = inAppProductID;
    return this;
  }

   /**
   * Get inAppProductID
   * @return inAppProductID
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getInAppProductID() {
    return inAppProductID;
  }

  public void setInAppProductID(String inAppProductID) {
    this.inAppProductID = inAppProductID;
  }

  public InAppProduct displayValue(String displayValue) {
    this.displayValue = displayValue;
    return this;
  }

   /**
   * Get displayValue
   * @return displayValue
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getDisplayValue() {
    return displayValue;
  }

  public void setDisplayValue(String displayValue) {
    this.displayValue = displayValue;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InAppProduct inAppProduct = (InAppProduct) o;
    return Objects.equals(this.g3Amount, inAppProduct.g3Amount) &&
        Objects.equals(this.inAppAmount, inAppProduct.inAppAmount) &&
        Objects.equals(this.inAppProductID, inAppProduct.inAppProductID) &&
        Objects.equals(this.displayValue, inAppProduct.displayValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(g3Amount, inAppAmount, inAppProductID, displayValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InAppProduct {\n");
    
    sb.append("    g3Amount: ").append(toIndentedString(g3Amount)).append("\n");
    sb.append("    inAppAmount: ").append(toIndentedString(inAppAmount)).append("\n");
    sb.append("    inAppProductID: ").append(toIndentedString(inAppProductID)).append("\n");
    sb.append("    displayValue: ").append(toIndentedString(displayValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

