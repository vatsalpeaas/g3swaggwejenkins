/*
 * G3 API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package co.peaas.map.g3apiswagger.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * PaymentReceipt
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-11-09T12:19:59.555+05:30")
public class PaymentReceipt {
  @SerializedName("orderId")
  private String orderId = null;

  @SerializedName("productId")
  private String productId = null;

  @SerializedName("purchaseTime")
  private Integer purchaseTime = null;

  @SerializedName("purchaseState")
  private Integer purchaseState = null;

  @SerializedName("purchaseToken")
  private String purchaseToken = null;

  @SerializedName("signature")
  private String signature = null;

  @SerializedName("autoRenewing")
  private Boolean autoRenewing = null;

  public PaymentReceipt orderId(String orderId) {
    this.orderId = orderId;
    return this;
  }

   /**
   * Get orderId
   * @return orderId
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public PaymentReceipt productId(String productId) {
    this.productId = productId;
    return this;
  }

   /**
   * Get productId
   * @return productId
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public PaymentReceipt purchaseTime(Integer purchaseTime) {
    this.purchaseTime = purchaseTime;
    return this;
  }

   /**
   * Get purchaseTime
   * @return purchaseTime
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getPurchaseTime() {
    return purchaseTime;
  }

  public void setPurchaseTime(Integer purchaseTime) {
    this.purchaseTime = purchaseTime;
  }

  public PaymentReceipt purchaseState(Integer purchaseState) {
    this.purchaseState = purchaseState;
    return this;
  }

   /**
   * Get purchaseState
   * @return purchaseState
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getPurchaseState() {
    return purchaseState;
  }

  public void setPurchaseState(Integer purchaseState) {
    this.purchaseState = purchaseState;
  }

  public PaymentReceipt purchaseToken(String purchaseToken) {
    this.purchaseToken = purchaseToken;
    return this;
  }

   /**
   * Get purchaseToken
   * @return purchaseToken
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getPurchaseToken() {
    return purchaseToken;
  }

  public void setPurchaseToken(String purchaseToken) {
    this.purchaseToken = purchaseToken;
  }

  public PaymentReceipt signature(String signature) {
    this.signature = signature;
    return this;
  }

   /**
   * Get signature
   * @return signature
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public PaymentReceipt autoRenewing(Boolean autoRenewing) {
    this.autoRenewing = autoRenewing;
    return this;
  }

   /**
   * Get autoRenewing
   * @return autoRenewing
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getAutoRenewing() {
    return autoRenewing;
  }

  public void setAutoRenewing(Boolean autoRenewing) {
    this.autoRenewing = autoRenewing;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentReceipt paymentReceipt = (PaymentReceipt) o;
    return Objects.equals(this.orderId, paymentReceipt.orderId) &&
        Objects.equals(this.productId, paymentReceipt.productId) &&
        Objects.equals(this.purchaseTime, paymentReceipt.purchaseTime) &&
        Objects.equals(this.purchaseState, paymentReceipt.purchaseState) &&
        Objects.equals(this.purchaseToken, paymentReceipt.purchaseToken) &&
        Objects.equals(this.signature, paymentReceipt.signature) &&
        Objects.equals(this.autoRenewing, paymentReceipt.autoRenewing);
  }

  @Override
  public int hashCode() {
    return Objects.hash(orderId, productId, purchaseTime, purchaseState, purchaseToken, signature, autoRenewing);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentReceipt {\n");
    
    sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    purchaseTime: ").append(toIndentedString(purchaseTime)).append("\n");
    sb.append("    purchaseState: ").append(toIndentedString(purchaseState)).append("\n");
    sb.append("    purchaseToken: ").append(toIndentedString(purchaseToken)).append("\n");
    sb.append("    signature: ").append(toIndentedString(signature)).append("\n");
    sb.append("    autoRenewing: ").append(toIndentedString(autoRenewing)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

