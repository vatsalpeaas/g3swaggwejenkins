package co.peaas.map.g3apiswagger;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import co.peaas.map.g3apiswagger.api.DefaultApi;
import co.peaas.map.g3apiswagger.model.AddCreditCardRequest;
import co.peaas.map.g3apiswagger.model.AddCreditCardResponse;
import co.peaas.map.g3apiswagger.model.Address;
import co.peaas.map.g3apiswagger.model.CCAddress;
import co.peaas.map.g3apiswagger.model.ChangeProfileRequest;
import co.peaas.map.g3apiswagger.model.ChangeProfileResponse;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentRequest;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentResponse;
import co.peaas.map.g3apiswagger.model.CreditCard;
import co.peaas.map.g3apiswagger.model.EmailVerificationRequest;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsByStateRequest;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsRequest;
import co.peaas.map.g3apiswagger.model.GetAccountHistoryRequest;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRespone;
import co.peaas.map.g3apiswagger.model.GetAllCreditCardsRequest;
import co.peaas.map.g3apiswagger.model.GetBalanceRequest;
import co.peaas.map.g3apiswagger.model.GetCdrRequest;
import co.peaas.map.g3apiswagger.model.GetConfigurationsRequest;
import co.peaas.map.g3apiswagger.model.GetCountryBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetLanRequest;
import co.peaas.map.g3apiswagger.model.GetLocalAccessNumberRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoIAPRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentMethodRequestRequest;
import co.peaas.map.g3apiswagger.model.GetProfileInfoRequest;
import co.peaas.map.g3apiswagger.model.GetProfileInfoResponse;
import co.peaas.map.g3apiswagger.model.GetRateRequest;
import co.peaas.map.g3apiswagger.model.GetRechargeAmountsRequest;
import co.peaas.map.g3apiswagger.model.GetSpecialOfferRequest;
import co.peaas.map.g3apiswagger.model.GetWorldBundlesRequest;
import co.peaas.map.g3apiswagger.model.IAPTopUpRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionResponse;
import co.peaas.map.g3apiswagger.model.RedeemVoucherRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendRequest;
import co.peaas.map.g3apiswagger.model.ResendEmailRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPResponse;
import co.peaas.map.g3apiswagger.model.SetUpCallForwardRequest;
import co.peaas.map.g3apiswagger.model.UpdatePushTokenRequest;
import co.peaas.map.g3apiswagger.model.VerifyOtpRequest;
import retrofit2.Call;
import retrofit2.Response;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * API tests for DefaultApi
 */
public class DefaultApiTest {

    private DefaultApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(DefaultApi.class);
    }


    /**
     * Add credit card info
     * <p>
     * Returns credit card info.
     */
    @Test
    public void addCreditCardPostTest() throws IOException {
        // TODO: test validations

        AddCreditCardRequest addCreditCard = new AddCreditCardRequest();
        CCAddress ccAddress = new CCAddress();
        ccAddress.setSelectedAddress("440867");
        CreditCard creditCard = new CreditCard();
        creditCard.setCcExpMonth("05");
        creditCard.setCcExpYear("2022");
        creditCard.setCcNumber("4242424242424242");
        creditCard.setCcOwnerName("Test Credit Card");
        creditCard.setCcvNumber("753");
        addCreditCard.setCreditCard(creditCard);
        addCreditCard.setAddress(ccAddress);
        addCreditCard.setCustomerId("504745");
        addCreditCard.setSelectedCC("Visa");
        addCreditCard.setAni("9011114500");
        addCreditCard.setApplicationVersion("1.0.3");
        addCreditCard.setDeviceID("trdef8b4b75cb110");
        addCreditCard.setPlatform("Android");

        // AddCreditCardResponse response = api.addCreditCardPost(addCreditCard);

        Call<AddCreditCardResponse> response = api.addCreditCardPost(addCreditCard);
        Response<AddCreditCardResponse> responseR = response.execute();
//        Log.d("AddCreditCard","onResponse"+responseR.body());
    }

    /**
     * changeProfile
     * <p>
     * Returns user profile related information.
     */
    @Test
    public void changeProfilePostTest() throws IOException {
        // TODO: test validations

        ChangeProfileRequest init = new ChangeProfileRequest();
        Address address = new Address();
        address.setSelectedAddress("439901");
        init.setAddress(address);
        init.setCustomerId("504745");
        init.setContactPhone("8866114543");
        init.setEmail("dot@com.com");
        init.setFirstName("Aaa");
        init.setLastName("Hhhhhh");
        init.setAni("8866114543");
        init.setApplicationVersion("1.0.3");
        init.setDeviceID("30e5djb4b75cb110");
        init.setPlatform("Android");
        // ChangeProfileResponse response = api.changeProfilePost(init);

        Call<ChangeProfileResponse> response = api.changeProfilePost(init);
        Response<ChangeProfileResponse> responseR = response.execute();
//        Log.d("initProvisioning", "onResponse" + responseR.body());

    }

    /**
     * ConfirmPayment
     * <p>
     * Returns a Confirm Payment related information.
     */
    @Test
    public void confirmPaymentPostTest() throws IOException {
        ConfirmPaymentRequest iaPTopUpRequest = new ConfirmPaymentRequest();
        iaPTopUpRequest.setCustomerID("471286");
        // ConfirmPaymentResponse response = api.confirmPaymentPost(iaPTopUpRequest);

        Call<ConfirmPaymentResponse> response = api.confirmPaymentPost(iaPTopUpRequest);
        Response<ConfirmPaymentResponse> responseR = response.execute();
        assertThat(responseR.body().getErrorCode(),is("200"));
//        Log.d("initProvisioning", "onResponse" + responseR.body());

        // TODO: test validations
    }

    /**
     * Email Verification Check
     * <p>
     * Email Verification Check
     */
    @Test
    public void emailVerificationCheckPostTest() {
        EmailVerificationRequest emailVerificationCheck = null;
        // EmailVerificationResponse response = api.emailVerificationCheckPost(emailVerificationCheck);

        // TODO: test validations
    }

    /**
     * GetAccessLocationsByState
     * <p>
     * Returns location related information.
     */
    @Test
    public void getAccessLocationsByStatePostTest() {
        GetAccessLocationsByStateRequest getAccessLocationsByState = null;
        // GetAccessLocationsResponse response = api.getAccessLocationsByStatePost(getAccessLocationsByState);

        // TODO: test validations
    }

    /**
     * GetAccessLocations
     * <p>
     * Returns location related information.
     */
    @Test
    public void getAccessLocationsPostTest() {
        GetAccessLocationsRequest getAccessLocations = null;
        // GetAccessLocationsResponse response = api.getAccessLocationsPost(getAccessLocations);

        // TODO: test validations
    }

    /**
     * GetAccountHistoryRequest
     * <p>
     * Returns a Account History related information.
     */
    @Test
    public void getAccountHistoryPostTest() {
        GetAccountHistoryRequest getAccountHistoryRequest = null;
        // GetAccountHistoryResponse response = api.getAccountHistoryPost(getAccountHistoryRequest);

        // TODO: test validations
    }

    /**
     * GetAllBundles
     * <p>
     * Returns bundles related information.
     */
    @Test
    public void getAllBundlesPostTest() throws IOException {
        GetAllBundlesRequest init = new GetAllBundlesRequest();
        init.setCustomerID("490571");
        init.setAni("9277208880");

        Call<GetAllBundlesRespone> response = api.getAllBundlesPost(init);
        Response<GetAllBundlesRespone> responseR = response.execute();
//        Log.d("GetAllBundlesRespone", "onResponse" + responseR.body());

        // GetAllBundlesRespone response = api.getAllBundlesPost(init);

        // TODO: test validations
    }

    /**
     * to get allcreditcardinfo
     * <p>
     * Returns credit card info
     */
    @Test
    public void getAllCreditCardsPostTest() {
        GetAllCreditCardsRequest getAllCreditCards = null;
        // GetAllCreditCardsResponse response = api.getAllCreditCardsPost(getAllCreditCards);

        // TODO: test validations
    }

    /**
     * to get Rechargeamount
     * <p>
     * for get Recharge amount
     */
    @Test
    public void getAutoRechargeAmountsPostTest() {
        GetRechargeAmountsRequest getAutoRechargeAmounts = null;
        // GetRechargeAmountsResponse response = api.getAutoRechargeAmountsPost(getAutoRechargeAmounts);

        // TODO: test validations
    }

    /**
     * GetBalance
     * <p>
     * Returns balance related information.
     */
    @Test
    public void getBalancePostTest() {
        GetBalanceRequest getBalance = new GetBalanceRequest();
        // GetBalanceResponse response = api.getBalancePost(getBalance);

        // TODO: test validations
    }

    /**
     * GetCdr
     * <p>
     * Returns Cdr related information.
     */
    @Test
    public void getCdrPostTest() {
        GetCdrRequest getCdr = null;
        // GetCdrResponse response = api.getCdrPost(getCdr);

        // TODO: test validations
    }

    /**
     * Get Configurations
     * <p>
     * GetConfigurations
     */
    @Test
    public void getConfigurationsPostTest() {
        GetConfigurationsRequest getConfigurations = null;
        // GetConfigurationsResponse response = api.getConfigurationsPost(getConfigurations);

        // TODO: test validations
    }

    /**
     * to get countrybundle
     * <p>
     * Returns countrybundle
     */
    @Test
    public void getCountryBundlesPostTest() {
        GetCountryBundlesRequest getCountryBundles = null;
        // GetCountryBundlesResponse response = api.getCountryBundlesPost(getCountryBundles);

        // TODO: test validations
    }

    /**
     * GetLan
     * <p>
     * Get LAN INfo.
     */
    @Test
    public void getLanPostTest() {
        GetLanRequest getLanRequest = null;
        // GetLanResponse response = api.getLanPost(getLanRequest);

        // TODO: test validations
    }

    /**
     * GetLocalAccessNumber
     * <p>
     * Returns LocalAccessNumber related information.
     */
    @Test
    public void getLocalAccessNumberPostTest() {
        GetLocalAccessNumberRequest getLocalAccessNumber = null;
        // GetLocalAccessNumberResponse response = api.getLocalAccessNumberPost(getLocalAccessNumber);

        // TODO: test validations
    }

    /**
     * Ge tPayment Info InApp
     * <p>
     * Get Payment Info.
     */
    @Test
    public void getPaymentInfoInAppPostTest() {
        GetPaymentInfoIAPRequest getPaymentInfoInApp = null;
        // GetPaymentInfoIAPResponse response = api.getPaymentInfoInAppPost(getPaymentInfoInApp);

        // TODO: test validations
    }

    /**
     * GetPaymentInfo
     * <p>
     * Returns a Payment Info related information.
     */
    @Test
    public void getPaymentInfoPostTest() {
        GetPaymentInfoRequest getPaymentInfoRequest = null;
        // GetPaymentInfoResponse response = api.getPaymentInfoPost(getPaymentInfoRequest);

        // TODO: test validations
    }

    /**
     * GetPaymentMethod
     * <p>
     * Returns a Payment Method related information.
     */
    @Test
    public void getPaymentMethodPostTest() {
        GetPaymentMethodRequestRequest getPaymentMethodRequestRequest = null;
        // GetPaymentMethodResponse response = api.getPaymentMethodPost(getPaymentMethodRequestRequest);

        // TODO: test validations
    }

    /**
     * GetProfileInfoRequest
     * <p>
     * Returns a user Profile related information.
     */
    @Test
    public void getProfileInfoPostTest() throws IOException {
        GetProfileInfoRequest getProfileInfoRequest = new GetProfileInfoRequest();
        getProfileInfoRequest.setCustomerId("490571");
//        getProfileInfoRequest.setDeviceID("959595959595959");
//        getProfileInfoRequest.setPlatform("Android");
//        getProfileInfoRequest.setAni("9277208880");

        Call<GetProfileInfoResponse> response = api.getProfileInfoPost(getProfileInfoRequest);
        Response<GetProfileInfoResponse> responseR = response.execute();
        Log.d("GetProfileInfoResponse", "onResponse" + responseR.body());

        // GetProfileInfoResponse response = api.getProfileInfoPost(getProfileInfoRequest);

        // TODO: test validations
    }

    /**
     * GetRate
     * <p>
     * Returns Rate related information.
     */
    @Test
    public void getRatePostTest() {
        GetRateRequest getRate = null;
        // GetRateResponse response = api.getRatePost(getRate);

        // TODO: test validations
    }

    /**
     * to get Rechargeamount
     * <p>
     * for get Recharge amount
     */
    @Test
    public void getRechargeAmountsPostTest() {
        GetRechargeAmountsRequest getRechargeAmounts = null;
        // GetRechargeAmountsResponse response = api.getRechargeAmountsPost(getRechargeAmounts);

        // TODO: test validations
    }

    /**
     * GetSpecialOfferRequest
     * <p>
     * Returns a Special Offer related information.
     */
    @Test
    public void getSpecialOfferPostTest() {
        GetSpecialOfferRequest getSpecialOfferRequest = null;
        // GetProfileInfoResponse response = api.getSpecialOfferPost(getSpecialOfferRequest);

        // TODO: test validations
    }

    /**
     * Get World Bundles
     * <p>
     * Get World Bundles
     */
    @Test
    public void getWorldBundlesPostTest() {
        GetWorldBundlesRequest getWorldBundles = null;
        // GetWorldBundlesResponse response = api.getWorldBundlesPost(getWorldBundles);

        // TODO: test validations
    }

    /**
     * IAP Subscribe
     * <p>
     * IAP Subscribe
     */
    @Test
    public void iAPSubscribePostTest() {
        IAPTopUpRequest iaPSubscribe = null;
        // ConfirmPaymentResponse response = api.iAPSubscribePost(iaPSubscribe);

        // TODO: test validations
    }

    /**
     * InApp Topup
     * <p>
     * InApp Topup
     */
    @Test
    public void iAPTopUpPostTest() {
        IAPTopUpRequest iaPTopUp = null;
        // ConfirmPaymentResponse response = api.iAPTopUpPost(iaPTopUp);

        // TODO: test validations
    }

    /**
     * InitProvisioning
     * <p>
     * Returns a user related information.
     */
    @Test
    public void initProvisioningPostTest() throws IOException {
        // TODO: test validations
        InitProvisionRequest init = new InitProvisionRequest();
        init.setApplicationVersion("1.0.2");
        init.setContactPhone("4168791223");
        init.setCountryCode("CA");
        init.setDeviceID("959595959595959");
        init.setEmail("mobiledev@peaas.co");
        init.setPlatform("Android");
        // InitProvisionResponse response = api.initProvisioningPost(init);
        Call<InitProvisionResponse> response = api.initProvisioningPost(init);
        Response<InitProvisionResponse> responseR = response.execute();
        Log.d("initProvisioning", "onResponse" + responseR.body());
//        assertThat(responseR.body().getErrorCode(),is("0"));
        assertThat(responseR.body().getEmail(),is("mobiledev@peaas.co"));
    }

    /**
     * InitTopUp
     * <p>
     * Returns a Init Top Up related information.
     */
    @Test
    public void initTopUpPostTest() {
        IAPTopUpRequest iaPTopUpRequest = null;
        // InitTopUpResponse response = api.initTopUpPost(iaPTopUpRequest);

        // TODO: test validations
    }

    /**
     * RedeemVoucher
     * <p>
     * Returns a Redeem Voucher related information.
     */
    @Test
    public void redeemVoucherPostTest() {
        RedeemVoucherRequest redeemVoucherRequest = null;
        // ConfirmPaymentResponse response = api.redeemVoucherPost(redeemVoucherRequest);

        // TODO: test validations
    }

    /**
     * ReferFriend
     * <p>
     * Returns confirmation message about refer a friend.
     */
    @Test
    public void referFriendPostTest() {
        ReferFriendRequest init = null;
        // ReferFriendResponse response = api.referFriendPost(init);

        // TODO: test validations
    }

    /**
     * Resend Email
     * <p>
     * ResendEmail
     */
    @Test
    public void resendEmailPostTest() {
        ResendEmailRequest resendEmail = null;
        // ResendEmailResponse response = api.resendEmailPost(resendEmail);

        // TODO: test validations
    }

    /**
     * ResendOtp
     * <p>
     * Returns a OTP related information.
     */
    @Test
    public void resendOtpPostTest() throws IOException {
        ResendOTPRequest init = new ResendOTPRequest();
        init.setCustomerId("471293");
        init.setContactPhone("4168791223");
        // ResendOTPResponse response = api.resendOtpPost(init);

        Call<ResendOTPResponse> response = api.resendOtpPost(init);

        Response<ResendOTPResponse> responseR = null;

        responseR = response.execute();
//            Log.d("initProvisioning", "onResponse" + responseR.body());


        // TODO: test validations
    }

    /**
     * to setupcallforwarding
     * <p>
     * for set call forwarding
     */
    @Test
    public void setUpCallForwardPostTest() {
        SetUpCallForwardRequest setUpCallForward = null;
        // SetUpCallForwardResponse response = api.setUpCallForwardPost(setUpCallForward);

        // TODO: test validations
    }

    /**
     * UpdatePushToken
     * <p>
     * Returns push token related information.
     */
    @Test
    public void updatePushTokenPostTest() {
        UpdatePushTokenRequest updatePushToken = null;
        // UpdatePushTokenResponse response = api.updatePushTokenPost(updatePushToken);

        // TODO: test validations
    }

    /**
     * verifyOtp
     * <p>
     * Returns a OTP related information.
     */
    @Test
    public void verifyOtpPostTest() {
        VerifyOtpRequest init = null;
        // VerifyOtpResponse response = api.verifyOtpPost(init);

        // TODO: test validations
    }

}
