package co.peaas.map.g3apiswagger;


import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import co.peaas.map.g3apiswagger.api.DefaultApi;
import co.peaas.map.g3apiswagger.model.AddCreditCardRequest;
import co.peaas.map.g3apiswagger.model.AddCreditCardResponse;
import co.peaas.map.g3apiswagger.model.ChangeProfileRequest;
import co.peaas.map.g3apiswagger.model.ChangeProfileResponse;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentRequest;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentResponse;
import co.peaas.map.g3apiswagger.model.EmailVerificationRequest;
import co.peaas.map.g3apiswagger.model.EmailVerificationResponse;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsRequest;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsResponse;
import co.peaas.map.g3apiswagger.model.GetAccountHistoryRequest;
import co.peaas.map.g3apiswagger.model.GetAccountHistoryResponse;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRespone;
import co.peaas.map.g3apiswagger.model.GetAllCreditCardsRequest;
import co.peaas.map.g3apiswagger.model.GetAllCreditCardsResponse;
import co.peaas.map.g3apiswagger.model.GetBalanceRequest;
import co.peaas.map.g3apiswagger.model.GetBalanceResponse;
import co.peaas.map.g3apiswagger.model.GetCdrRequest;
import co.peaas.map.g3apiswagger.model.GetCdrResponse;
import co.peaas.map.g3apiswagger.model.GetConfigurationsRequest;
import co.peaas.map.g3apiswagger.model.GetConfigurationsResponse;
import co.peaas.map.g3apiswagger.model.GetCountryBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetCountryBundlesResponse;
import co.peaas.map.g3apiswagger.model.GetLanRequest;
import co.peaas.map.g3apiswagger.model.GetLanResponse;
import co.peaas.map.g3apiswagger.model.GetLocalAccessNumberResponse;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoIAPRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoIAPResponse;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoResponse;
import co.peaas.map.g3apiswagger.model.GetPaymentMethodRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentMethodResponse;
import co.peaas.map.g3apiswagger.model.GetProfileInfoRequest;
import co.peaas.map.g3apiswagger.model.GetProfileInfoResponse;
import co.peaas.map.g3apiswagger.model.GetRateRequest;
import co.peaas.map.g3apiswagger.model.GetRateResponse;
import co.peaas.map.g3apiswagger.model.GetRechargeAmountsRequest;
import co.peaas.map.g3apiswagger.model.GetRechargeAmountsResponse;
import co.peaas.map.g3apiswagger.model.GetSpecialOfferRequest;
import co.peaas.map.g3apiswagger.model.GetSpecialOfferResponse;
import co.peaas.map.g3apiswagger.model.GetWorldBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetWorldBundlesResponse;
import co.peaas.map.g3apiswagger.model.IAPTopUpRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionResponse;
import co.peaas.map.g3apiswagger.model.InitTopUpResponse;
import co.peaas.map.g3apiswagger.model.RedeemVoucherRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendResponse;
import co.peaas.map.g3apiswagger.model.ResendEmailRequest;
import co.peaas.map.g3apiswagger.model.ResendEmailResponse;
import co.peaas.map.g3apiswagger.model.ResendOTPRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPResponse;
import co.peaas.map.g3apiswagger.model.SetUpCallForwardRequest;
import co.peaas.map.g3apiswagger.model.SetUpCallForwardResponse;
import co.peaas.map.g3apiswagger.model.UpdatePushTokenRequest;
import co.peaas.map.g3apiswagger.model.UpdatePushTokenResponse;
import co.peaas.map.g3apiswagger.model.VerifyOtpRequest;
import co.peaas.map.g3apiswagger.model.VerifyOtpResponse;
import retrofit2.Call;
import retrofit2.Response;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * API tests for DefaultApi
 */
public class DefaultApiTestNew {

    private DefaultApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(DefaultApi.class);
    }

    /**
     * Add credit card info
     *
     * Returns credit card info.
     */
    @Test
    public void addCreditCardPostTest() throws IOException{
        AddCreditCardRequest addCreditCard = new AddCreditCardRequest();
		
		
        // AddCreditCardResponse response = api.addCreditCardPost(addCreditCard);
		
		Call<AddCreditCardResponse> response = api.addCreditCardPost(addCreditCard);
		Response<AddCreditCardResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * changeProfile
     *
     * Returns user profile related information.
     */
    @Test
    public void changeProfilePostTest() throws IOException{
        ChangeProfileRequest init = new ChangeProfileRequest();
		
		
        // ChangeProfileResponse response = api.changeProfilePost(init);
		
		Call<ChangeProfileResponse> response = api.changeProfilePost(init);
		Response<ChangeProfileResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * ConfirmPayment
     *
     * Returns a Confirm Payment related information.
     */
    @Test
    public void confirmPaymentPostTest() throws IOException{
        ConfirmPaymentRequest confirmPaymentRequest = new ConfirmPaymentRequest();
		
		
        // ConfirmPaymentResponse response = api.confirmPaymentPost(confirmPaymentRequest);
		
		Call<ConfirmPaymentResponse> response = api.confirmPaymentPost(confirmPaymentRequest);
		Response<ConfirmPaymentResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * Email Verification Check
     *
     * Email Verification Check
     */
    @Test
    public void emailVerificationCheckPostTest() throws IOException{
        EmailVerificationRequest emailVerificationCheck = new EmailVerificationRequest();
		
		
        // EmailVerificationResponse response = api.emailVerificationCheckPost(emailVerificationCheck);
		
		Call<EmailVerificationResponse> response = api.emailVerificationCheckPost(emailVerificationCheck);
		Response<EmailVerificationResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetAccessLocationsByState
     *
     * Returns location related information.
     */
    @Test
    public void getAccessLocationsByStatePostTest() throws IOException{
        GetAccessLocationsRequest getAccessLocationsByState = new GetAccessLocationsRequest();
		
		
        // GetAccessLocationsResponse response = api.getAccessLocationsByStatePost(getAccessLocationsByState);
		
		Call<GetAccessLocationsResponse> response = api.getAccessLocationsByStatePost(getAccessLocationsByState);
		Response<GetAccessLocationsResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetAccessLocations
     *
     * Returns location related information.
     */
    @Test
    public void getAccessLocationsPostTest() throws IOException{
        GetAccessLocationsRequest getAccessLocations = new GetAccessLocationsRequest();
		
		
        // GetAccessLocationsResponse response = api.getAccessLocationsPost(getAccessLocations);
		
		Call<GetAccessLocationsResponse> response = api.getAccessLocationsPost(getAccessLocations);
		Response<GetAccessLocationsResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetAccountHistoryRequest
     *
     * Returns a Account History related information.
     */
    @Test
    public void getAccountHistoryPostTest() throws IOException{
        GetAccountHistoryRequest getAccountHistoryRequest = new GetAccountHistoryRequest();
		
		
        // GetAccountHistoryResponse response = api.getAccountHistoryPost(getAccountHistoryRequest);
		
		Call<GetAccountHistoryResponse> response = api.getAccountHistoryPost(getAccountHistoryRequest);
		Response<GetAccountHistoryResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetAllBundles
     *
     * Returns bundles related information.
     */
    @Test
    public void getAllBundlesPostTest() throws IOException{
        GetAllBundlesRequest init = new GetAllBundlesRequest();
		
		
        // GetAllBundlesRespone response = api.getAllBundlesPost(init);
		
		Call<GetAllBundlesRespone> response = api.getAllBundlesPost(init);
		Response<GetAllBundlesRespone> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * to get allcreditcardinfo
     *
     * Returns credit card info
     */
    @Test
    public void getAllCreditCardsPostTest() throws IOException{
        GetAllCreditCardsRequest getAllCreditCards = new GetAllCreditCardsRequest();
		
		
        // GetAllCreditCardsResponse response = api.getAllCreditCardsPost(getAllCreditCards);
		
		Call<GetAllCreditCardsResponse> response = api.getAllCreditCardsPost(getAllCreditCards);
		Response<GetAllCreditCardsResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * to get Rechargeamount
     *
     * for get Recharge amount
     */
    @Test
    public void getAutoRechargeAmountsPostTest() throws IOException{
        GetRechargeAmountsRequest getAutoRechargeAmounts = new GetRechargeAmountsRequest();
		
		
        // GetRechargeAmountsResponse response = api.getAutoRechargeAmountsPost(getAutoRechargeAmounts);
		
		Call<GetRechargeAmountsResponse> response = api.getAutoRechargeAmountsPost(getAutoRechargeAmounts);
		Response<GetRechargeAmountsResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetBalance
     *
     * Returns balance related information.
     */
    @Test
    public void getBalancePostTest() throws IOException{
        GetBalanceRequest getBalance = new GetBalanceRequest();
		
		
        // GetBalanceResponse response = api.getBalancePost(getBalance);
		
		Call<GetBalanceResponse> response = api.getBalancePost(getBalance);
		Response<GetBalanceResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetCdr
     *
     * Returns Cdr related information.
     */
    @Test
    public void getCdrPostTest() throws IOException{
        GetCdrRequest getCdr = new GetCdrRequest();
		
		
        // GetCdrResponse response = api.getCdrPost(getCdr);
		
		Call<GetCdrResponse> response = api.getCdrPost(getCdr);
		Response<GetCdrResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * Get Configurations
     *
     * GetConfigurations
     */
    @Test
    public void getConfigurationsPostTest() throws IOException{
        GetConfigurationsRequest getConfigurations = new GetConfigurationsRequest();
		
		
        // GetConfigurationsResponse response = api.getConfigurationsPost(getConfigurations);
		
		Call<GetConfigurationsResponse> response = api.getConfigurationsPost(getConfigurations);
		Response<GetConfigurationsResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * to get countrybundle
     *
     * Returns countrybundle
     */
    @Test
    public void getCountryBundlesPostTest() throws IOException{
        GetCountryBundlesRequest getCountryBundles = new GetCountryBundlesRequest();
		
		
        // GetCountryBundlesResponse response = api.getCountryBundlesPost(getCountryBundles);
		
		Call<GetCountryBundlesResponse> response = api.getCountryBundlesPost(getCountryBundles);
		Response<GetCountryBundlesResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetLan
     *
     * Get LAN INfo.
     */
    @Test
    public void getLanPostTest() throws IOException{
        GetLanRequest getLanRequest = new GetLanRequest();
		
		
        // GetLanResponse response = api.getLanPost(getLanRequest);
		
		Call<GetLanResponse> response = api.getLanPost(getLanRequest);
		Response<GetLanResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetLocalAccessNumber
     *
     * Returns LocalAccessNumber related information.
     */
    @Test
    public void getLocalAccessNumberPostTest() throws IOException{
        GetAccessLocationsRequest getLocalAccessNumber = new GetAccessLocationsRequest();
		
		
        // GetLocalAccessNumberResponse response = api.getLocalAccessNumberPost(getLocalAccessNumber);
		
		Call<GetLocalAccessNumberResponse> response = api.getLocalAccessNumberPost(getLocalAccessNumber);
		Response<GetLocalAccessNumberResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * Ge tPayment Info InApp
     *
     * Get Payment Info.
     */
    @Test
    public void getPaymentInfoInAppPostTest() throws IOException{
        GetPaymentInfoIAPRequest getPaymentInfoInApp = new GetPaymentInfoIAPRequest();
		
		
        // GetPaymentInfoIAPResponse response = api.getPaymentInfoInAppPost(getPaymentInfoInApp);
		
		Call<GetPaymentInfoIAPResponse> response = api.getPaymentInfoInAppPost(getPaymentInfoInApp);
		Response<GetPaymentInfoIAPResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetPaymentInfo
     *
     * Returns a Payment Info related information.
     */
    @Test
    public void getPaymentInfoPostTest() throws IOException{
        GetPaymentInfoRequest getPaymentInfoRequest = new GetPaymentInfoRequest();
		
		
        // GetPaymentInfoResponse response = api.getPaymentInfoPost(getPaymentInfoRequest);
		
		Call<GetPaymentInfoResponse> response = api.getPaymentInfoPost(getPaymentInfoRequest);
		Response<GetPaymentInfoResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetPaymentMethod
     *
     * Returns a Payment Method related information.
     */
    @Test
    public void getPaymentMethodPostTest() throws IOException{
        GetPaymentMethodRequest getPaymentMethodRequestRequest = new GetPaymentMethodRequest();
		
		
        // GetPaymentMethodResponse response = api.getPaymentMethodPost(getPaymentMethodRequestRequest);
		
		Call<GetPaymentMethodResponse> response = api.getPaymentMethodPost(getPaymentMethodRequestRequest);
		Response<GetPaymentMethodResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetProfileInfoRequest
     *
     * Returns a user Profile related information.
     */
    @Test
    public void getProfileInfoPostTest() throws IOException{
        GetProfileInfoRequest getProfileInfoRequest = new GetProfileInfoRequest();
		
		
        // GetProfileInfoResponse response = api.getProfileInfoPost(getProfileInfoRequest);
		
		Call<GetProfileInfoResponse> response = api.getProfileInfoPost(getProfileInfoRequest);
		Response<GetProfileInfoResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetRate
     *
     * Returns Rate related information.
     */
    @Test
    public void getRatePostTest() throws IOException{
        GetRateRequest getRate = new GetRateRequest();
		
		
        // GetRateResponse response = api.getRatePost(getRate);
		
		Call<GetRateResponse> response = api.getRatePost(getRate);
		Response<GetRateResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * to get Rechargeamount
     *
     * for get Recharge amount
     */
    @Test
    public void getRechargeAmountsPostTest() throws IOException{
        GetRechargeAmountsRequest getRechargeAmounts = new GetRechargeAmountsRequest();
		
		
        // GetRechargeAmountsResponse response = api.getRechargeAmountsPost(getRechargeAmounts);
		
		Call<GetRechargeAmountsResponse> response = api.getRechargeAmountsPost(getRechargeAmounts);
		Response<GetRechargeAmountsResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * GetSpecialOfferRequest
     *
     * Returns a Special Offer related information.
     */
    @Test
    public void getSpecialOfferPostTest() throws IOException{
        GetSpecialOfferRequest getSpecialOfferRequest = new GetSpecialOfferRequest();
		
		
        // GetSpecialOfferResponse response = api.getSpecialOfferPost(getSpecialOfferRequest);
		
		Call<GetSpecialOfferResponse> response = api.getSpecialOfferPost(getSpecialOfferRequest);
		Response<GetSpecialOfferResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * Get World Bundles
     *
     * Get World Bundles
     */
    @Test
    public void getWorldBundlesPostTest() throws IOException{
        GetWorldBundlesRequest getWorldBundles = new GetWorldBundlesRequest();
		
		
        // GetWorldBundlesResponse response = api.getWorldBundlesPost(getWorldBundles);
		
		Call<GetWorldBundlesResponse> response = api.getWorldBundlesPost(getWorldBundles);
		Response<GetWorldBundlesResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * IAP Subscribe
     *
     * IAP Subscribe
     */
    @Test
    public void iAPSubscribePostTest() throws IOException{
        IAPTopUpRequest iaPSubscribe = new IAPTopUpRequest();
		
		
        // ConfirmPaymentResponse response = api.iAPSubscribePost(iaPSubscribe);
		
		Call<ConfirmPaymentResponse> response = api.iAPSubscribePost(iaPSubscribe);
		Response<ConfirmPaymentResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * InApp Topup
     *
     * InApp Topup
     */
    @Test
    public void iAPTopUpPostTest() throws IOException{
        IAPTopUpRequest iaPTopUp = new IAPTopUpRequest();
		
		
        // ConfirmPaymentResponse response = api.iAPTopUpPost(iaPTopUp);
		
		Call<ConfirmPaymentResponse> response = api.iAPTopUpPost(iaPTopUp);
		Response<ConfirmPaymentResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * InitProvisioning
     *
     * Returns a user related information.
     */
    @Test
    public void initProvisioningPostTest() throws IOException{
        InitProvisionRequest init = new InitProvisionRequest();
		
		
        // InitProvisionResponse response = api.initProvisioningPost(init);
		
		Call<InitProvisionResponse> response = api.initProvisioningPost(init);
		Response<InitProvisionResponse> responseR = response.execute();
//        Log.d("responseR",""+responseR.body());
//		assertThat(responseR.body().getErrorCode(),is("0"));
		assertThat(responseR.body().getEmail(),is("mobiledev@peaas.co"));

		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     *
     * InitTopUp
     *
     * Returns a Init Top Up related information.
     */
    @Test
    public void initTopUpPostTest() throws IOException{
        IAPTopUpRequest iaPTopUpRequest = new IAPTopUpRequest();
		
		
        // InitTopUpResponse response = api.initTopUpPost(iaPTopUpRequest);
		
		Call<InitTopUpResponse> response = api.initTopUpPost(iaPTopUpRequest);
		Response<InitTopUpResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * RedeemVoucher
     *
     * Returns a Redeem Voucher related information.
     */
    @Test
    public void redeemVoucherPostTest() throws IOException{
        RedeemVoucherRequest redeemVoucherRequest = new RedeemVoucherRequest();
		
		
        // ConfirmPaymentResponse response = api.redeemVoucherPost(redeemVoucherRequest);
		
		Call<ConfirmPaymentResponse> response = api.redeemVoucherPost(redeemVoucherRequest);
		Response<ConfirmPaymentResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * ReferFriend
     *
     * Returns confirmation message about refer a friend.
     */
    @Test
    public void referFriendPostTest() throws IOException{
        ReferFriendRequest init = new ReferFriendRequest();
		
		
        // ReferFriendResponse response = api.referFriendPost(init);
		
		Call<ReferFriendResponse> response = api.referFriendPost(init);
		Response<ReferFriendResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * Resend Email
     *
     * ResendEmail
     */
    @Test
    public void resendEmailPostTest() throws IOException{
        ResendEmailRequest resendEmail = new ResendEmailRequest();
		
		
        // ResendEmailResponse response = api.resendEmailPost(resendEmail);
		
		Call<ResendEmailResponse> response = api.resendEmailPost(resendEmail);
		Response<ResendEmailResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * ResendOtp
     *
     * Returns a OTP related information.
     */
    @Test
    public void resendOtpPostTest() throws IOException{
        ResendOTPRequest init = new ResendOTPRequest();
		
		
        // ResendOTPResponse response = api.resendOtpPost(init);
		
		Call<ResendOTPResponse> response = api.resendOtpPost(init);
		Response<ResendOTPResponse> responseR = response.execute();
//        Log.d("initProvisioning", "onResponse" + responseR.body());
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * to setupcallforwarding
     *
     * for set call forwarding
     */
    @Test
    public void setUpCallForwardPostTest() throws IOException{
        SetUpCallForwardRequest setUpCallForward = new SetUpCallForwardRequest();
		
		
        // SetUpCallForwardResponse response = api.setUpCallForwardPost(setUpCallForward);
		
		Call<SetUpCallForwardResponse> response = api.setUpCallForwardPost(setUpCallForward);
		Response<SetUpCallForwardResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * UpdatePushToken
     *
     * Returns push token related information.
     */
    @Test
    public void updatePushTokenPostTest() throws IOException{
        UpdatePushTokenRequest updatePushToken = new UpdatePushTokenRequest();
		
		
        // UpdatePushTokenResponse response = api.updatePushTokenPost(updatePushToken);
		
		Call<UpdatePushTokenResponse> response = api.updatePushTokenPost(updatePushToken);
		Response<UpdatePushTokenResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
    /**
     * verifyOtp
     *
     * Returns a OTP related information.
     */
    @Test
    public void verifyOtpPostTest() throws IOException{
        VerifyOtpRequest init = new VerifyOtpRequest();
		
		
        // VerifyOtpResponse response = api.verifyOtpPost(init);
		
		Call<VerifyOtpResponse> response = api.verifyOtpPost(init);
		Response<VerifyOtpResponse> responseR = response.execute();
		assertThat(responseR.body().getErrorCode(),is("0"));
		
		//Hello Hi.
		
        // TODO: test validations
    }
}